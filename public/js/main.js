(function () {
    var tableManager = {

        table: null,

        updateTable: function () {
            var atheneumId = $('#atheneum option:selected').val();
            $('.atheneumId').attr('value', atheneumId);
            this.table.ajax.url('/atheneum/' + atheneumId).load();
        },

        fillTable: function(inAtheneumId){
            this.table = $('#main-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                paging: true,
                info: false,
                lengthChange: true,
                ajax: {
                    url: '/atheneum/' + inAtheneumId,
                    dataSrc: function(json){
                        var data = [];
                        var inData = json.data;
                        inData.forEach(function(elem){
                            data.push([
                                elem.Name,
                                elem.Authors.join(", "),
                                '<a href="/book/'+elem.Id+'" class="edit-link" data-name="'+elem.Name+'" data-authors="'+elem.Authors+'">edit</a>',
                                '<a href="/book/'+elem.Id+'" class="delete-link" data-name="'+elem.Name+'" data-authors="'+elem.Authors+'">delete</a>',
                            ]);
                        });
                        return data;
                    }
                }
            });

            var manager = this;
            $('.atheneum-btn').click(function (e) {
                e.preventDefault();
                clearMessages();
                manager.updateTable();
                return false;
            });
        },
    };

    $(document).ready(function() {
        (function(){
            $.get('/atheneum/',
                function (data, textStatus, jqXHR) {
                    data.forEach(function(item){
                        $('<option/>', {
                            text: item.Name,
                            value: item.Id,
                        }).appendTo($('#atheneum'));
                    });
                    var atheneumId = data[0].Id;
                    $('.atheneumId').attr('value', atheneumId);
                    tableManager.fillTable(atheneumId);
                }
            );
        })();
    });

    function clearMessages() {
        $('.error-message').html('');
        $('.success-message').html('');
    }

    function successCallback(jqXHR, $form) {
        var responseJson = JSON.parse(jqXHR['responseText']);
        clearMessages();
        if (responseJson && 'Error' in responseJson) {
            $('.error-message').html('<p>'+responseJson['Error']+'</p>');
        } else {
            $('.success-message').html('<p>New book has been added</p>')
            $form[0].reset();
            tableManager.updateTable();
        }
    }

    $('.form-create').submit(function (e) {
        e.preventDefault()
        var $form = $(this);
        var formData = $form.serialize();
        $.post($form.attr('action'), formData,
            function (data, textStatus, jqXHR) {
                successCallback(jqXHR, $form);
            }
        );

        return false;
    });

    $('.form-update').submit(function (e) {
        e.preventDefault()
        var $form = $(this);
        var formData = $form.serialize();

        $.ajax({
            url: $form.attr('action'),
            type: 'PUT',
            data: formData,
            complete: function (jqXHR, textStatus) { },
            success: function (data, textStatus, jqXHR) {
                successCallback(jqXHR, $form);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error during sending request');
            }
        });
        return false;
    });

    $('.cancel-btn').click(function (e) {
        e.preventDefault();
        $('.form-update-row').addClass('g-hidden');
        $('.form-create-row').removeClass('g-hidden');
        return false;
    });

    $(document).on('click', '.edit-link', function (e) {
        e.preventDefault();
        $('.form-update').attr('action', $(this).attr('href'));
        $('.form-update-row').removeClass('g-hidden');
        $('.form-create-row').addClass('g-hidden');

        $('#name').val($(this).data('name'));
        $('#authors').val($(this).data('authors'));
        return false;
    });

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var csrf = $("[name='authenticity_token']").val();
        $.ajax({
            url: $(this).attr('href'),
            type: 'DELETE',
            data: {"gorilla.csrf.Token": csrf},
            complete: function (jqXHR, textStatus) {
                console.log('delete complete');
            },
            success: function (data, textStatus, jqXHR) {
                console.log('delete success');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('delete error');
            }
        });
        return false;
    });
})()
