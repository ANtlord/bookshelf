package main

import (
	"os"
	"fmt"
	"bookshelf/config"
	"bookshelf/controller"
	"net/http"
	"path"
	log "github.com/Sirupsen/logrus"
	"github.com/go-gem/gem"
	"github.com/go-gem/middleware-csrf"
	"github.com/gorilla/csrf"
)

var csrfMiddleware = csrfmidware.New(
	[]byte("32-byte-long-auth-key"),
	csrf.RequestHeader("Authenticity-Token"),
	csrf.FieldName("authenticity_token"),
	csrf.Secure(false),
)

func main() {
	conf := config.GetConfig()

	srv := gem.New(":8080")

	router := gem.NewRouter()
	router.Use(csrfMiddleware)
	router.ServeFiles(
		path.Join(fmt.Sprintf("%s*filepath", conf.StaticUrl)),
		http.Dir(conf.StaticDir),
	)
	router.GET("/", controller.Index)

	router.GET("/atheneum/", controller.GetAtheneumList)
	router.GET("/atheneum/:id", controller.GetAtheneum)
	router.POST("/atheneum/", controller.CreateAtheneum)
	router.GET("/book/:id", controller.GetBook)
	router.POST("/book/", controller.CreateBook)
	router.PUT("/book/:id", controller.UpdateBook)
	router.DELETE("/book/:id", controller.DeleteBook)

	logger := log.New()
	logger.Level = log.DebugLevel
	logger.Out = os.Stdout

	srv.SetLogger(logger)
	log.Println(srv.ListenAndServe(router.Handler()))
}
