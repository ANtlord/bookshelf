package model

import (
	"bookshelf/config"

	"github.com/go-pg/pg"
)

var db *pg.DB = nil

func init() {
	if db == nil {
		dbconf := config.GetConfig().Db
		db = pg.Connect(&pg.Options{
			User:     dbconf.User,
			Addr:     dbconf.Host,
			Password: dbconf.Pass,
			Database: dbconf.Base,
		})
	}
}

func GetDB() *pg.DB {
	return db
}

type Atheneum struct {
	Id   int64
	Name string
}

type Book struct {
	Id         int64
	AtheneumId int64
	Name       string
	Authors    []string
}
