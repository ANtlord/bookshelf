# Installation

* go to project directory
* `go get`
* `cp config.yml.dist config.yml`
* Edit config.yml considering your system
* `go run ./migrations/*.go init`
* `go run ./migrations/*.go`
* `go build`
* `bookshelf`
