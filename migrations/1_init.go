package main

import (
	"fmt"

	"github.com/go-pg/migrations"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		fmt.Println("creating table books and atheneums")
		_, err := db.Exec(`
			CREATE TABLE atheneums(
				id serial PRIMARY KEY,
				name varchar(255) not null
			);
			CREATE TABLE books(
				id serial PRIMARY KEY,
				atheneum_id int,
				name varchar(255) not null,
				authors jsonb,
				FOREIGN KEY (atheneum_id) REFERENCES atheneums (id)
			);
			CREATE INDEX books_atheneum_id_0 ON books USING btree (atheneum_id);
			INSERT INTO "atheneums" ("name") VALUES ('Art');
			INSERT INTO "atheneums" ("name") VALUES ('Documentary');
			INSERT INTO "atheneums" ("name") VALUES ('Scientific');
		`)
		return err
	}, func(db migrations.DB) error {
		fmt.Println("dropping table books and atheneums")
		_, err := db.Exec(`
			DROP TABLE books;
			DROP TABLE atheneum;
		`)
		return err
	})
}
