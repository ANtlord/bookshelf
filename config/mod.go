package config

import (
	"os"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var settings *Settings = nil

const CONFIG_FILE = "config.yml"

func GetConfig() Settings {
	if _, err := os.Stat(CONFIG_FILE); os.IsNotExist(err) {
		panic(err.Error())
	}
	if settings == nil {
		data, err := ioutil.ReadFile(CONFIG_FILE)
		if err != nil {
			panic(err.Error())
		}
		settings = &Settings{}
		err2 := yaml.Unmarshal([]byte(data), settings)
		if err2 != nil {
			panic(err2.Error())
		}
	}
	if settings == nil {
		panic("settings is not initialized")
	}
	return *settings
}

type Settings struct {
	StaticDir   string `yaml:"static_dir"`
	StaticUrl   string `yaml:"static_url"`
	TemplateDir string `yaml:"template_dir"`
	Db          struct {
		User string
		Pass string
		Host string
		Base string
	}
}
