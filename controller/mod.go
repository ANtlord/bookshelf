package controller

import (
	"bookshelf/config"
	"bookshelf/model"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-gem/gem"
	"github.com/go-pg/pg/orm"
	"github.com/gorilla/csrf"
)

const (
	PAGINATE_BY     = 10
	NO_RESULT_ERROR = "pg: no rows in result set"
	SERVER_ERROR    = "Internal Server Error"
)

func throwError(ctx *gem.Context, err error) bool {
	if err != nil {
		ctx.Logger().Error(err.Error())
		ctx.Error(SERVER_ERROR, 500)
		return true
	}
	return false
}

func Index(ctx *gem.Context) {
	ts := gem.NewTemplates(config.GetConfig().TemplateDir)
	layoutName := "main"
	ts.SetLayout(layoutName)

	tpl, err := ts.Render(layoutName, "index")
	if throwError(ctx, err) {
		return
	}
	tpl.Execute(ctx.Response, map[string]interface{}{
		csrf.TemplateTag: csrf.TemplateField(ctx.Request),
	})
}

type URLValuesModel struct {
	Id   int
	Name string
}

func GetAtheneumList(ctx *gem.Context) {
	var atheneums []model.Atheneum
	query := getList(ctx, &atheneums)
	err := query.Select()
	if throwError(ctx, err) {
		return
	}
	ctx.JSON(200, atheneums)
}

func CreateAtheneum(ctx *gem.Context) {
	err := ctx.Request.ParseForm()
	if throwError(ctx, err) {
		return
	}
	db := model.GetDB()

	object := model.Atheneum{
		Name: ctx.Request.FormValue("name"),
	}
	err = db.Insert(&object)
	if throwError(ctx, err) {
		return
	}
	path := fmt.Sprintf("/atheneum/%d", object.Id)
	ctx.Response.Header().Set("Location", path)
	ctx.JSON(201, CreatedResponse{path})
}

func getList(ctx *gem.Context, data interface{}) *orm.Query {
	urlQuery := ctx.Request.URL.Query()
	db := model.GetDB()

	query := orm.NewQuery(db, data)

	start := urlQuery.Get("start")
	if len(start) > 0 {
		offset, err := strconv.Atoi(start)
		if err == nil {
			query = query.Offset(offset)
		}
	}

	length := urlQuery.Get("length")
	if len(length) > 0 {
		limit, err := strconv.Atoi(length)
		if err == nil {
			query = query.Limit(limit)
		}
	}
	return query
}

func GetAtheneum(ctx *gem.Context) {
	var books []model.Book
	AtheneumId := ctx.UserValue("id")
	query := getList(ctx, &books)
	query = query.Where("atheneum_id = ?", AtheneumId)
	err := query.Select()

	if throwError(ctx, err) {
		return
	}

	drawVal := ctx.Request.URL.Query().Get("draw")
	draw := 1
	if len(drawVal) > 0 {
		draw, _ = strconv.Atoi(drawVal)
	}

	db := model.GetDB()
	count, err := db.Model(&model.Book{}).Where("atheneum_id = ?", AtheneumId).Count()
	if throwError(ctx, err) {
		return
	}

	if len(books) == 0 {
		ctx.JSON(200, map[string]interface{}{
			"draw":            draw,
			"recordsFiltered": count,
			"recordsTotal":    count,
			"data":            []interface{}{},
		})
	} else {
		ctx.JSON(200, map[string]interface{}{
			"draw":            draw,
			"recordsFiltered": count,
			"recordsTotal":    count,
			"data":            books,
		})
	}
}

func getById(ctx *gem.Context, obj interface{}, id string) int {
	db := model.GetDB()
	if err := db.Model(obj).Where("id = ?", id).First(); err != nil {
		errorText := err.Error()
		if errorText == NO_RESULT_ERROR {
			return 404
			ctx.NotFound()
		} else {
			ctx.Logger().Errorf(err.Error())
			return 500
			ctx.Error(SERVER_ERROR, 500)
		}
	}
	return 200
}

func Int64(v interface{}) (int64, error) {
	switch value := v.(type) {
	case int64:
		return value, nil
	case string:
		return strconv.ParseInt(v.(string), 10, 32)
	}
	return 0, fmt.Errorf("unsupport to convert type %T to int64", v)
}

func GetBook(ctx *gem.Context) {
	id, err := gem.String(ctx.UserValue("id"))
	if throwError(ctx, err) {
		return
	}
	var book = model.Book{}
	res := getById(ctx, &book, id)

	if res == 404 {
		ctx.NotFound()
	} else if res == 500 {
		ctx.Error(SERVER_ERROR, 500)
	} else {
		ctx.JSON(200, book)
	}
}

type ValidationError struct {
	Error string
}

func newBook(ctx *gem.Context) *model.Book {
	err := ctx.Request.ParseForm()
	if throwError(ctx, err) {
		return nil
	}

	atheneumIdVal := ctx.Request.FormValue("atheneum_id")
	if atheneumIdVal == "" {
		ctx.JSON(200, ValidationError{"atheneum_id is required field"})
		return nil
	}
	atheneumId, err := strconv.ParseInt(atheneumIdVal, 10, 32)
	if throwError(ctx, err) {
		return nil
	}
	authors := strings.Split(ctx.Request.FormValue("authors"), ",")
	for i := 0; i < len(authors); i++ {
		authors[i] = strings.TrimSpace(authors[i])
	}

	name := strings.TrimSpace(ctx.Request.FormValue("name"))
	if len(name) == 0 {
		ctx.JSON(200, ValidationError{"name field is required"})
		return nil
	}

	object := &model.Book{
		Name:       name,
		AtheneumId: atheneumId,
		Authors:    authors,
	}
	return object
}

type CreatedResponse struct {
	url string
}

func saveBook(ctx *gem.Context, book *model.Book) bool {
	db := model.GetDB()
	if err := db.Insert(book); err != nil {
		errorText := err.Error()

		if strings.Contains(errorText, "violates foreign key constraint") {
			ctx.JSON(200, ValidationError{
				fmt.Sprintf("Atheneum with id %d doesn't exist", book.AtheneumId),
			})
		} else {
			ctx.Logger().Errorf(err.Error())
			ctx.Error(SERVER_ERROR, 500)
		}
		return false
	}
	return true
}

func CreateBook(ctx *gem.Context) {
	object := newBook(ctx)
	if object == nil {
		return
	}
	if !saveBook(ctx, object) {
		return
	}

	ctx.JSON(201, CreatedResponse{
		fmt.Sprintf("/book/%d", object.Id),
	})
}

func UpdateBook(ctx *gem.Context) {
	err := ctx.ParseForm()
	if throwError(ctx, err) {
		return
	}
	idval, err := gem.String(ctx.UserValue("id"))
	if throwError(ctx, err) {
		return
	}
	id, err := strconv.ParseInt(idval, 10, 32)
	if throwError(ctx, err) {
		return
	}

	db := model.GetDB()
	book := &model.Book{}

	res := getById(ctx, &book, idval)
	if res == 500 {
		ctx.Error(SERVER_ERROR, 500)
		return
	}
	isBookNew := book.Id == 0

	book = newBook(ctx)
	if book == nil {
		return
	}

	book.Id = id

	if isBookNew {
		if !saveBook(ctx, book) {
			return
		}
		ctx.JSON(201, "")
	} else {
		err := db.Update(&book)
		if err != nil {
			return
		}
		ctx.JSON(200, "")
	}
}

func DeleteBook(ctx *gem.Context) {
	id, err := Int64(ctx.UserValue("id"))
	if throwError(ctx, err) {
		return
	}
	db := model.GetDB()

	object := model.Book{Id: id}
	if err := db.Delete(&object); err != nil {
		errorText := err.Error()
		if errorText == NO_RESULT_ERROR {
			ctx.NotFound()
		} else {
			ctx.Logger().Error(errorText)
			ctx.Error(SERVER_ERROR, 500)
		}
		return
	}
	ctx.JSON(204, "")
}
